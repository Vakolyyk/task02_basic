/*

 */
package com.vakolyuk;

import java.util.Scanner;

/** .
 * My first maven project
 */
class Programm {
    /** .
     * Entering the beginning and the end of the interval
     * Entering size of Fibonacci numbers set
     * Search for the largest odd number and the largest even number
     * Search for the percentage of odd and even Fibonacci numbers
     * @param args tag of main function
     */
    public static void main(final String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Enter the beginning of the interval:");
        int start = in.nextInt();
        System.out.println("Enter the end of the interval:");
        int end = in.nextInt();

        print(start, end);
        sum(start, end);

        System.out.println("Enter size of Fibonacci numbers set:");
        int countNum = in.nextInt();
        int maxOdd = 0;
        int maxEven = 0;
        int countOdd = 0;
        int countEven = 0;
        System.out.print("Fibonacci numbers: ");
        for (int i = 0; i <= countNum - 1; i++) {
            System.out.print(fibonacci(i) + " ");
            if (fibonacci(i) % 2 == 0) {
                if (maxEven < fibonacci(i)) {
                    maxEven = fibonacci(i);
                }
                countEven++;
            } else {
                if (maxOdd < fibonacci(i)) {
                    maxOdd = fibonacci(i);
                }
                countOdd++;
            }
        }
        final double percent = 100.0;
        double pcEven = percent * countEven / countNum;
        double pcOdd = percent * countOdd / countNum;
        System.out.println("\nF1 = " + maxOdd);
        System.out.println("Per cent of odd numbers = " + pcOdd);
        System.out.println("F2 = " + maxEven);
        System.out.println("Per cent of even numbers = " + pcEven);
    }
    /** .
     * Method of outputting odd numbers from beginning to end of the
     * interval and even from end to beginning
     * @param start the beginning of interval
     * @param end the end of interval
     */
    public static void print(final int start, final int end) {
        for (int i = start; i <= end; i++) {
            if (i % 2 == 1) {
                System.out.print(i + " ");
            }
        }
        System.out.println("\n");
        for (int i = end; i >= start; i--) {
            if (i % 2 == 0) {
                System.out.print(i + " ");
            }
        }
        System.out.println("\n");
    }

    /**.
     * Method of outputting the sum of odd and even numbers
     * @param start the beginning of interval
     * @param end the end of interval
     */
    public static void sum(final int start, final int end) {
        int sumOfOdd = 0;
        int sumOfEven = 0;
        for (int i = start; i <= end; i++) {
            if (i % 2 == 0) {
                sumOfEven += i;
            } else {
                sumOfOdd += i;
            }
        }
        System.out.println("Sum of odd numbers:" + sumOfOdd);
        System.out.println("Sum of even numbers:" + sumOfEven);
    }

    /**.
     * Method of Fibonacci numbers
     * @param n sequence number Fibonacci
     * @return number of Fibonacci
     */
    public static int fibonacci(final int n) {
        if (n == 0) {
            return 0;
        }
        if (n == 1) {
            return 1;
        } else {
            return fibonacci(n - 1) + fibonacci(n - 2);
        }
    }
}

